#!/bin/sh

set -eu

./deps.sh

build="$1"
home="$HOME"

# Install a vim addon using Debian's vim addon manager.
# $1: Addon to install.
vim_addon_install()
(
	addon="$1"
	state="$(vim-addons -q -- status "$addon")"
	targetdir="$home/.vim"
	read -r name status <<EOF
		$state
EOF
	case "$status" in
		installed)
			# Always remove if it is already installed.
			# vim-addon-manager creates symlinks so an update to the
			# package might miss new files.

			# The -z option is not actually fully quiet.
			vim-addons \
				-t "$targetdir" \
				-z \
				-- remove "$addon" \
				>/dev/null
			vim-addons \
				-t "$targetdir" \
				-z \
				-- install "$addon" \
				>/dev/null
			;;
		removed)
			vim-addons \
				-t "$targetdir" \
				-z \
				-- install "$addon" \
				>/dev/null
			;;
		*)
			printf 'ERROR: %s: unexpected status %s\n' \
				"$addon" "$status" >&2
			exit 1
			;;
	esac
)

[ -f "$build/vimrc" ] || exit 1

# Config.
[ -d "$home/.vim" ] || mkdir -- "$home/.vim"
chmod -- 0700 "$home/.vim"

cp -p -- "$build/vimrc" "$home/.vimrc"
cp -pR -- "$build/vim"/* "$home/.vim"

# Some packages in Debian bullseye are not packaged for use with vim's native
# packagement. Instead it ships with its own addon manager, so use that to
# install those packages instead.
if [ -f /usr/share/vim/registry/vim-airline.yaml ]; then
	vim_addon_install airline
	vim_addon_install airline-themes
fi

if [ -f /usr/share/vim/registry/vim-ledger.yaml ]; then
	vim_addon_install ledger
fi
