#!/bin/sh

set -eu
umask 0077

hash m4

host="$1"

. src/config.sh
[ -e "host/$host/config.sh" ] && . "host/$host/config.sh"

[ -d build ] && rm -r -- build
mkdir -- build

colorcolumn=src/colorcolumn
if [ -e "host/$host/colorcolumn" ]; then
	colorcolumn="host/$host/colorcolumn"
fi

extra_config="host/$host/extra_config"

m4 -EP \
	-D "__AIRLINE_TABLINE_FNAMECOLLAPSE__=[[[$airline_tabline_fnamecollapse]]]" \
	-D "__AIRLINE_TABLINE_FORMATTER__=[[[$airline_tabline_formatter]]]" \
	-D "__AIRLINE_THEME__=[[[$airline_theme]]]" \
	-D "__ALE_LSP__=[[[$ale_lsp]]]" \
	-D "__BUFFER_COMPLETION__=[[[$buffer_completion]]]" \
	-D "__COLORCOLUMN__=[[[$colorcolumn]]]" \
	-D "__COLORSCHEME__=[[[$colorscheme]]]" \
	-D "__CURSORLINE__=[[[$cursorline]]]" \
	-D "__EXTRA_CONFIG__=[[[$extra_config]]]" \
	-D "__SPELLLANG_EN__=[[[$spelllang_en]]]" \
	-D "__SPELLLANG_SECOND__=[[[$spelllang_second]]]" \
	-D "__SYSTEM_CLIPBOARD__=[[[$system_clipboard]]]" \
	-D "__TRANSPARENT_BACKGROUND__=[[[$transparent_background]]]" \
	-- src/vimrc \
	>build/vimrc

cp -LR -- src/vim build
cp -LR -- src/plugins/vim-cpp-enhanced-highlight/after build/vim
cp -LR -- \
	src/plugins/plantuml-syntax/ftdetect \
	src/plugins/plantuml-syntax/ftplugin \
	src/plugins/plantuml-syntax/indent \
	src/plugins/plantuml-syntax/syntax \
	build/vim
cp -LR -- src/plugins/vim-bbye/plugin build/vim
