#!/bin/sh

set -eu

hash ctags-universal || apt-get -qy install universal-ctags
if ! hash vim || ! vim --version | grep -q -- +python3; then
	apt-get -qy install vim-nox
fi
[ -d /usr/share/doc/vim-airline ] || apt-get -qy install vim-airline
[ -d /usr/share/doc/vim-ale ] || apt-get -qy install vim-ale
[ -d /usr/share/doc/vim-ctrlp ] || apt-get -qy install vim-ctrlp
[ -d /usr/share/doc/vim-ledger ] || apt-get -qy install vim-ledger
[ -d /usr/share/doc/vim-scripts ] || apt-get -qy install vim-scripts
